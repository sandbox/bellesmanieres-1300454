<?php
/**
 * @file
 * Admin form elements.
 */


/**
 * Profile related form elements for eloqua admin form.
 * @param
 * $elems_id = String identifying the elements to return.
 * Optionally can also pass the original $form and $form_state as references
 */
function eloqua_profile_admin_form_elements($elems_id, &$form, &$form_state) {
  $func = '_eloqua_profile_admin_form_elements_' . $elems_id;
  $elems = $func(&$form, &$form_state);
  //Let others manipulate the settings.
  drupal_alter('eloqua_profile_admin_form_elements', &$elems, $elems_id);
  return $elems;
}

function _eloqua_profile_admin_form_elements_eloqua_admin_form(&$form, &$form_state) {
  $elems['profile'] = array(
    '#type' => 'fieldset',
    '#title' => t('Profile Settings'),
    '#description' => t('Settings applicable to core profile integration.'),
  );
  $elems['profile'][ELOQUA_PROFILE_VARIABLE_NAME_REGISTER_FORM_ENABLED] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable on registration form'),
    '#description' => t('Enable eloqua on registration form'),
    '#default_value' => ELOQUA_PROFILE_VARIABLE_REGISTER_FORM_ENABLED,
  );
  $elems['profile'][ELOQUA_PROFILE_VARIABLE_NAME_PROFILE_FORM_ENABLED] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable on profile form'),
    '#description' => t('Enable eloqua on profile form'),
    '#default_value' => ELOQUA_PROFILE_VARIABLE_PROFILE_FORM_ENABLED,
  );
  return $elems;
}

function _eloqua_profile_admin_form_elements_user_register(&$form, &$form_state) {
  $parent = ELOQUA_PROFILE_VARIABLE_NAME_REGISTER_FORM_SETTINGS;
  $title = t('User registration');
  return _eloqua_profile_admin_form_elements_builder(&$form, &$form_state, $parent, $title);
}

function _eloqua_profile_admin_form_elements_user_profile_form(&$form, &$form_state) {
  $parent = ELOQUA_PROFILE_VARIABLE_NAME_PROFILE_FORM_SETTINGS;
  $title = t('User profile');
  return _eloqua_profile_admin_form_elements_builder(&$form, &$form_state, $parent, $title);
}

function _eloqua_profile_admin_form_elements_builder(&$form, &$form_state, $parent, $title) {
  //Get saved settings.
  $defaults = variable_get($parent, array());
  //Pass parent to all the helpers functions.
  $defaults['parent'] = $parent;
  //Wrapper fieldset.
  $elems[$parent] = array(
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#title' => $title,
    '#tree' => TRUE,
  );
//Main Eloqua form ID.
  $elems[$parent]['eloqua_id'] = array(
    '#type' => 'textfield',
    '#description' => t('The form name as it is defined in Eloqua'),
    '#title' => t('Eloqua Form Name'),
    '#required' => TRUE,
    '#weight' => -1,
    '#default_value' => (isset($defaults['eloqua_id'])) ? $defaults['eloqua_id'] : '',
  );
  $elems[$parent]['original_id'] = array(
    '#type' => 'value',
    '#value' => 'user_profile_form',
  );
  //Get account/profile fields
  $fields = _eloqua_profile_admin_form_get_fields($defaults, FALSE);
  //Additional hidden fields.
  $hidden = _eloqua_profile_admin_form_hidden_fields($defaults, &$form, &$form_state);
  $settings = array_merge($fields, $hidden);
  $elems[$parent] = array_merge($elems[$parent], $settings);
  return $elems;
}

/*
 * Helper function to get fields. Returns for elements.
 */

function _eloqua_profile_admin_form_get_fields($defaults, $register = FALSE) {
  $fields = array();
  //Standard user fields.
  $fields['account']['name'] = t('Username');
  $fields['account']['mail'] = t('E-mail address');
  if (variable_get('user_signatures', 0) && module_exists('comment') && !$register) {
    $fields['signature_settings']['signature'] = t('Signature');
  }
  //Profile fields.
  $q = db_query("SELECT * FROM {profile_fields}");
  while ($r = db_fetch_object($q)) {
    if (!$register || $r->register == 1) {
      $fields[$r->category][$r->name] = t($r->title);
    }
  }
  //Build as form elems array.
  $elems = array();
  foreach ($fields as $cat => $field) {
    $elems[$cat] = array(
      '#type' => 'fieldset',
      '#title' => t('!cat', array('!cat' => ucfirst($cat))),
    );
    foreach ($field as $key => $val) {
      $elems[$cat][$key] = array(
        '#type' => 'textfield',
        '#title' => $val,
        '#default_value' => (isset($defaults[$cat][$key]) && !empty($defaults[$cat][$key])) ? $defaults[$cat][$key] : $key,
      );
    }
  }
  return $elems;
}

/*
 * Build the hidden fields wrapper for ahah adding of fields.
 */

function _eloqua_profile_admin_form_hidden_fields($defaults, &$form, &$form_state) {
  $parent = $defaults['parent'];
  if (isset($form_state[$parent . '_hidden_count'])) {
    $count = $form_state[$parent . '_hidden_count'];
  }
  else {
    $count = max(1, empty($defaults['hidden']['hidden_wrapper']['hd_fields']) ? 1 : count($defaults['hidden']['hidden_wrapper']['hd_fields']));
  }
  $elems['hidden'] = array(
    '#type' => 'fieldset',
    '#title' => t('Additional hidden fields'),
    '#parents' => array($parent, 'hidden'),
//    '#tree' => TRUE,
  );
  // Add a wrapper.
  $elems['hidden']['hidden_wrapper'] = array(
    '#weight' => -4,
    '#prefix' => '<div class="clear-block" id="elq-profile-hd-fields-wrapper-' . $parent . '">',
    '#suffix' => '</div>',
    '#parents' => array($parent, 'hidden', 'hidden_wrapper'),
  );


  $elems['hidden']['hidden_wrapper']['hd_fields'] = array(
    '#prefix' => '<div id="elq-profile-hd-fields-' . $parent . '">',
    '#suffix' => '</div>',
    '#parents' => array($parent, 'hidden', 'hidden_wrapper', 'hd_fields'),
  );
  // Add the current choices to the form.
  for ($delta = 0; $delta < $count; $delta++) {
    $default_label = isset($defaults['hidden']['hidden_wrapper']['hd_fields'][$delta]['hd_label']) ? $defaults['hidden']['hidden_wrapper']['hd_fields'][$delta]['hd_label'] : '';
    $default_value = isset($defaults['hidden']['hidden_wrapper']['hd_fields'][$delta]['hd_value']) ? $defaults['hidden']['hidden_wrapper']['hd_fields'][$delta]['hd_value'] : '';
    $label = isset($form_state['values'][$parent]['hidden']['hidden_wrapper']['hd_fields'][$delta]['hd_label']) ? $form_state['values'][$parent]['hidden']['hidden_wrapper']['hd_fields'][$delta]['hd_label'] : $default_label;
    $value = isset($form_state['values'][$parent]['hidden']['hidden_wrapper']['hd_fields'][$delta]['hd_value']) ? $form_state['values'][$parent]['hidden']['hidden_wrapper']['hd_fields'][$delta]['hd_value'] : $default_value;
    $elems['hidden']['hidden_wrapper']['hd_fields'][$delta] = _eloqua_profile_admin_form_hidden_field_element($parent, $delta, $label, $value);
  }
  $elems['hidden']['hidden_wrapper']['hd_fields_more'] = array(
    '#type' => 'button',
    '#name' => $parent . '_add_more',
    '#value' => t('Add another item'),
    '#ahah' => array(
      'path' => 'eloqua_profile_admin_form_hidden_fields/js/' . $parent,
      'wrapper' => 'elq-profile-hd-fields-' . $parent,
      'method' => 'replace',
      'effect' => 'fade',
    ),
    '#submit' => array('_eloqua_profile_admin_form_hidden_fields_submit'),
  );

  return $elems;
}

/*
 * Build an individual "hidden" field.
 */

function _eloqua_profile_admin_form_hidden_field_element($parent, $delta, $label = '', $value = '') {
  $elem['hd_label'] = array(
    '#type' => 'textfield',
    '#default_value' => $label,
    '#size' => 20,
    '#parents' => array($parent, 'hidden', 'hidden_wrapper', 'hd_fields', $delta, 'hd_label'),
    '#title' => t('Eloqua field name'),
    '#prefix' => t('Hidden field @n', array('@n' => ($delta + 1))),
  );
  $elem['hd_value'] = array(
    '#type' => 'textfield',
    '#default_value' => $value,
    '#size' => 20,
    '#parents' => array($parent, 'hidden', 'hidden_wrapper', 'hd_fields', $delta, 'hd_value'),
    '#title' => t('Value'),
  );

  return $elem;
}

/**
 * Menu callback for AHAH additions.
 */
function eloqua_profile_admin_form_hidden_fields_js($parent) {
  //Usual ahah blurb.
  $form_state = array('storage' => NULL, 'submitted' => FALSE);
  $form_build_id = $_POST['form_build_id'];
  $form = form_get_cache($form_build_id, $form_state);
  $args = $form['#parameters'];
  $form_id = array_shift($args);
  $form['#redirect'] = FALSE;
  $form['#post'] = $_POST;
  $form['#programmed'] = FALSE;
  $form_state['post'] = $_POST;
  drupal_process_form($form_id, $form, $form_state);
  $form_state['submit_handlers'] = $form['#submit'];
  $form_state[$parent . '_hidden_count'] = count($form_state['values'][$parent]['hidden']['hidden_wrapper']['hd_fields']) + 1;
  $form = drupal_rebuild_form($form_id, $form_state, $args, $form_build_id);
  //Define element from parent.
  $hidden = $form['eloqua_profile'][$parent]['hidden']['hidden_wrapper']['hd_fields'];
  unset($hidden['#prefix'], $hidden['#suffix']);
  $output = theme('status_messages') . drupal_render($hidden);
  drupal_json(array('status' => TRUE, 'data' => $output));
}

/**
 * Submit for non js add more hidden fields.
 */
function _eloqua_profile_admin_form_hidden_fields_submit($form, &$form_state) {
  //Get the parent from clicked button.
  $parent = str_replace('_add_more', '', $form_state['clicked_button']['#name']);
  if ($form_state['values'][$parent]['hidden']['hidden_wrapper']['hd_fields_more']) {
    $form_state[$parent . '_hidden_count'] = count($form_state['values'][$parent]['hidden']['hidden_wrapper']['hd_fields']) + 1;
  }
//  unset($form_state['submit_handlers']);
//  form_execute_handlers('submit', $form, $form_state);
}

